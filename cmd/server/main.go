package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"

	"bitbucket.org/orangeparis/runner/config"
	"bitbucket.org/orangeparis/runner/workspace"

	"bitbucket.org/orangeparis/runner/web/app"
)

var templates *template.Template

func main() {

	// get config
	config.Load()

	// create the workspace pool if needed
	wk, err := workspace.WorkSpacePoolFromConfig()
	if err != nil {
		log.Fatal(err.Error())
	}
	err = wk.Init()
	if err != nil {
		log.Fatal(err.Error())
	}

	// declare web applcation routes
	app.Routes()

	// starts the web server
	port := config.GetString("web.port")
	addr := fmt.Sprintf(":%s", port)

	log.Printf("start server at %s", addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}
