package workspace

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

/*

	handle a pool of workspaces


*/

var sampleScript = `
# my script
echo "hello there" > result.out
`

// FsPool   a pool of workspaces base on the file system
// implements Workspace : Name() Root() , Add() , Get() List() Run()
type FsPool struct {
	root    string // base directory  eg /tmp/runs/12
	scripts string // directory to find commands scripts eg /tmp/scripts
}

func NewFsPool(root string, commands string) (pool *FsPool, err error) {

	if root == "" {
		root = "/tmp/runs"
	}
	if commands == "" {
		commands = "/tmp/scripts"
	}
	return &FsPool{root: root, scripts: commands}, nil
}

// Init pool , create directories for home and scripts , add a demo script to lib
func (w *FsPool) Init() (err error) {

	// create pool directory
	err = os.MkdirAll(w.root, os.ModePerm)
	if err != nil {
		log.Printf("Cannot create workspace pool at %s", w.root)
		return err
	}
	// create scripts directory
	err = os.MkdirAll(w.scripts, os.ModePerm)
	if err != nil {
		log.Printf("Canot create scripts base at %s", w.scripts)
		return err
	}
	err = w.AddScript("sampleScript", []byte(sampleScript))
	return err
}

// Home : return home directory
func (w *FsPool) Home() string {
	return w.root
}

// Scripts : return scripts directory
func (w *FsPool) Scripts() string {
	return w.scripts
}

func (w *FsPool) ListScript() (files []string) {

	info, err := ioutil.ReadDir(w.Scripts())
	if err != nil {
		return files
	}
	for _, f := range info {
		//fmt.Println(f.Name())
		files = append(files, f.Name())
	}
	return files

}

func (w *FsPool) GetScript(name string) (content []byte, err error) {

	filename := filepath.Join(w.Scripts(), name)
	content, err = ioutil.ReadFile(filename)
	return content, err

}

func (w *FsPool) AddScript(name string, data []byte) error {
	fname := filepath.Join(w.Scripts(), name)
	err := ioutil.WriteFile(fname, data, 0755)
	return err
}

// NewSpace : create a new workspace and add a script
func (w *FsPool) NewSpace(script string, name string) (space *FsWorkspace, err error) {

	if name == "" {
		name = GenerateRunId()
	}

	// create the workspace
	space, err = NewFsWorkspace(name, w.Home())
	if err != nil {
		return space, nil
	}
	// copy the script
	code, err := w.GetScript(script)
	if err != nil {
		return space, err
	}
	err = space.AddCommand(script, code)

	return space, err
}

// Space return a existing space
func (w *FsPool) Space(name string) (space *FsWorkspace, err error) {

	// create the workspace reference
	p := filepath.Join(w.Home(), name)
	space = &FsWorkspace{name: name, root: p}
	if !space.IsExists() {
		m := fmt.Sprintf("workspace [%s] not found", name)
		err = errors.New(m)
	}
	return space, err
}

// Clear pool : remove home and script directories
func (w *FsPool) Clear() {

	if w.Home() != "" {
		os.RemoveAll(w.Home())
	}
	if w.Scripts() != "" {
		os.RemoveAll(w.Scripts())
	}
}
