// +build windows

package workspace

/*
	a shell execution engine for windows ( powershell )


*/

import (
	"bytes"
	"os/exec"
)

type Shell struct {
	Shell string
	Dir   string
}

func NewShell(dir string) *Shell {
	ps, _ := exec.LookPath("powershell.exe")
	return &PowerShell{
		Shell: ps,
		Dir:   dir,
	}
}

func (p *Shell) Execute(args ...string) (stdOut string, stdErr string, err error) {
	args = append([]string{"-NoProfile", "-NonInteractive"}, args...)
	cmd := exec.Command(p.Shell, args...)

	if p.Dir != "" {
		// NEED to convert to windows path ???
		cmd.Dir = p.Dir
	}

	var stdout bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	err = cmd.Run()
	stdOut, stdErr = stdout.String(), stderr.String()
	return
}

/*

cmd:= exec.Command("git", "log")
cmd.Dir = "your/intended/working/directory"
out, err := cmd.Output()
*/

// func main() {
//         posh := New()
//         stdout, stderr, err := posh.Execute("$OutputEncoding = [Console]::OutputEncoding; (Get-VMSwitch).Name")

//         fmt.Println(stdout)
//         fmt.Println(stderr)

//         if err != nil {
//                 fmt.Println(err)
//         }
// }

// func main() {
// 	posh := New()

// 	fmt.Println("With encoding change:")
// 	stdout, stderr, err := posh.Execute(
// 			"$test = \"Przełąś\"\n" +
// 			"$old = [Console]::OutputEncoding\n" +
// 			"[Console]::OutputEncoding = [Text.Encoding]::UTF8\n" +
// 			"[Console]::OutputEncoding\n" +
// 			"$test\n" +
// 			"[Console]::OutputEncoding = $old")
// 	fmt.Println(stdout)
// 	fmt.Println(stderr)
// 	if err != nil {
// 			fmt.Println(err)
// 	}

// 	fmt.Println("Without encoding change:")
// 	stdout, stderr, err = posh.Execute(
// 			"$test = \"Przełąś\"\n" +
// 			"[Console]::OutputEncoding\n" +
// 			"$test")
// 	fmt.Println(stdout)
// 	fmt.Println(stderr)
// 	if err != nil {
// 			fmt.Println(err)
// 	}
// }
