package workspace_test

import (
	"testing"

	"bitbucket.org/orangeparis/runner/config"
	"bitbucket.org/orangeparis/runner/workspace"
)

func TestGenerateRunId(t *testing.T) {

	r := workspace.GenerateRunId()
	println(r)
	if len(r) != 16 {
		t.Fail()
	}

}

func TestNewWorkSpaceFromConfig(t *testing.T) {

	config.Load()
	wk, err := workspace.NewWorkSpaceFromConfig("run1")
	if err != nil {
		t.Fail()
		return
	}
	if wk.Name() != "run1" {
		t.Fail()
		return

	}

}

func TestPoolFromConfig(t *testing.T) {

	config.Load()
	pool, err := workspace.WorkSpacePoolFromConfig()
	if err != nil {
		t.Fail()
		return
	}
	err = pool.Init()
	if err != nil {
		t.Fail()
		return
	}
	defer pool.Clear()

	// add script
	err = pool.AddScript("sampleScript2", []byte(sampleScript))
	if err != nil {
		t.Fail()
		return
	}

	wk1, err := pool.NewSpace("sampleScript", "")
	if err != nil {
		t.Fail()
		return
	}
	_ = wk1

}
