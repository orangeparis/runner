package workspace

import (
	"crypto/rand"
	"fmt"
	"log"

	"bitbucket.org/orangeparis/runner/config"
)

func GenerateRunId() string {

	b := make([]byte, 8)
	_, err := rand.Read(b)
	if err != nil {
		log.Fatal(err)
	}
	uuid := fmt.Sprintf("%x%x%x", b[0:4], b[4:6], b[6:8])
	//fmt.Println(uuid)
	return uuid
}

func WorkSpacePoolFromConfig() (*FsPool, error) {

	root := config.GetString("runner.runs")
	scripts := config.GetString("runner.commands")
	pool, err := NewFsPool(root, scripts)
	return pool, err

}

func NewWorkSpaceFromConfig(name string) (*FsWorkspace, error) {

	if name == "" {
		name = GenerateRunId()
	}
	root := config.GetString("runner.runs")
	space, err := NewFsWorkspace(name, root)
	return space, err

}

// func NewWorkSpacePoolFromConfig() (*WorkspacePool, error) {

// 	// commands := config.GetString("runner.commands")
// 	home := config.GetString("runner.runs")

// 	ws, err := NewWorkspacePool(home)

// 	return ws, err

// }
