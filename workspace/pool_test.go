package workspace_test

import (
	"os"
	"testing"

	"bitbucket.org/orangeparis/runner/workspace"
)

// var sampleScript = `
// # my script
// echo "hello there" > result.out
// `

func clearPool() {
	// remove /tmp/run1 directory
	os.RemoveAll("/tmp/runs")
	os.RemoveAll("/tmp/scripts")

}

func TestFsPool(t *testing.T) {

	clearPool()
	defer clearPool()

	pool, err := workspace.NewFsPool("/tmp/runs", "/tmp/scripts")
	if err != nil {
		t.Fail()
		return
	}

	home := pool.Home()
	if home != "/tmp/runs" {
		t.Fail()
	}
	scripts := pool.Scripts()
	if scripts != "/tmp/scripts" {
		t.Fail()
	}

	err = pool.Init()
	if err != nil {
		t.Fail()
	}

}

func TestFsPoolNewWorkspace(t *testing.T) {

	clearPool()
	defer clearPool()

	pool, err := workspace.NewFsPool("/tmp/runs", "/tmp/scripts")
	if err != nil {
		t.Fail()
		return
	}

	err = pool.Init()
	if err != nil {
		t.Fail()
	}

	// Add a script to the pool library
	err = pool.AddScript("sampleScript", []byte(sampleScript))
	if err != nil {
		t.Fail()
		return
	}

	// create a new workspace ( run ) with a script
	wk, err := pool.NewSpace("sampleScript", "")
	if err != nil {
		t.Fail()
		return
	}
	wk_id := wk.Name()

	// retreave a workspace
	wk2, err := pool.Space(wk_id)
	if err != nil {
		t.Fail()
		return
	}

	_ = wk
	_ = wk2

}
