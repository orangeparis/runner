package workspace

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

/*

	handle run


*/

// FsWorkSpace  a file system workspace
// implements Workspace : Name() Root() , Add() , Get() List() Run()
type FsWorkspace struct {
	name string // name of the workspace eg 12
	root string // base directory  eg /tmp/runs/12
}

func NewFsWorkspace(name, base string) (workspace *FsWorkspace, err error) {

	if base == "" {
		base = "/tmp"
	}
	p := filepath.Join(base, name)
	// create workspace directory
	err = os.MkdirAll(p, os.ModePerm)
	if err != nil {
		log.Printf("Canot create workspace at %s", p)
		return workspace, err
	}
	return &FsWorkspace{name: name, root: p}, nil

}

func (w *FsWorkspace) Name() string {
	return w.name
}
func (w *FsWorkspace) Root() string {
	return w.root
}
func (w *FsWorkspace) Add(name string, data []byte) error {
	fname := filepath.Join(w.Root(), name)
	err := ioutil.WriteFile(fname, data, 0644)
	return err
}

func (w *FsWorkspace) List() (files []string) {

	info, err := ioutil.ReadDir(w.Root())
	if err != nil {
		return files
	}
	for _, f := range info {
		//fmt.Println(f.Name())
		files = append(files, f.Name())
	}
	return files

}

func (w *FsWorkspace) Get(name string) (content []byte) {

	filename := filepath.Join(w.Root(), name)
	content, _ = ioutil.ReadFile(filename)
	return content

}

func (w *FsWorkspace) AddCommand(name string, data []byte) error {
	fname := filepath.Join(w.Root(), name)
	err := ioutil.WriteFile(fname, data, 0755)
	return err
}

func (w *FsWorkspace) Run(cmd string, args []string) (err error) {

	// execute command line
	r := NewShell(w.Root())
	log.Printf("run command %s , in workspace %s", cmd, w.Root())
	var line []string
	line = append(line, cmd)
	line = append(line, args...)
	stdout, stderr, err := r.Execute(line...)
	if err != nil {
		return err
	}
	_ = stdout
	_ = stderr
	return err

}

//
//
//

// IsExists : test if root directory exists on disk
func (w *FsWorkspace) IsExists() bool {

	// check root directory exists
	_, err := os.Stat(w.Root())
	if err != nil {
		return os.IsNotExist(err)
	} else {
		return true
	}
}

func (w *FsWorkspace) Delete() (err error) {

	// delete workspace

	return err

}
