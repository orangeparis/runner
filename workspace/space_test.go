package workspace_test

import (
	"os"
	"testing"

	"bitbucket.org/orangeparis/runner/workspace"
)

var sampleScript = `
# my script
echo "hello there" > result.out
`

func clear() {
	// remove /tmp/run1 directory
	os.RemoveAll("/tmp/run1")
}

func TestFsWorkspace(t *testing.T) {

	clear()
	defer clear()

	space, err := workspace.NewFsWorkspace("run1", "/tmp")
	if err != nil {
		t.Fail()
		return
	}

	name := space.Name()
	if name != "run1" {
		t.Fail()
	}
	root := space.Root()
	if root != "/tmp/run1" {
		t.Fail()
	}

}

func TestFsWorkspaceAdd(t *testing.T) {

	// remove /tmp/run1 directory
	clear()
	defer clear()

	space, err := workspace.NewFsWorkspace("run1", "/tmp")
	if err != nil {
		t.Fail()
		return
	}

	err = space.Add("hello.txt", []byte("I am the hello.txt content\n"))
	if err != nil {
		t.Fail()
	}

	content := space.Get("hello.txt")
	if string(content) != "I am the hello.txt content\n" {
		t.Fail()
		return
	}

	space.Add("bye.txt", []byte("I am the bye.txt content\n"))

	err = space.AddCommand("deploy_vm.exe", []byte(sampleScript))
	if err != nil {
		t.Fail()
	}

	l := space.List()
	if len(l) != 3 {
		t.Fail()
	}

}

func TestFsWorkspaceRun(t *testing.T) {

	// remove /tmp/run1 directory
	clear()
	defer clear()

	space, err := workspace.NewFsWorkspace("run1", "/tmp")
	if err != nil {
		t.Fail()
		return
	}

	err = space.AddCommand("deploy_vm.exe", []byte(sampleScript))
	if err != nil {
		t.Fail()
	}

	var args = []string{"/usr/local/bin/bash", "deploy_vm.exe"}
	err = space.Run("", args)

	l := space.List()
	if len(l) != 2 {
		t.Fail()
	}

}
