package workspace

/*

	A workspace is an isolated directory to run commands

*/

// Workspace : a unique directory to execute command in isolation
type Workspace interface {
	New(Id string)

	Name() string //  return Id of workspace ( run id )

	Root() string // base directory of the workspace  eg /tmp/runs/1

	Add(name string, content []byte) // add a file to workspace
	Get(name string) []byte          // return content of the file
	List() []string                  // list files of workspace

	AddCommand(name string, content []byte)

	Run(cmd string, args string) // run a command
}

// Pool :  a set of workspaces
type Pool interface {
	Home() string // base directory of the work pool
	Scripts()
	Init() error
	Clear()

	NewSpace(script string, name string) *Workspace // create a new workspace

	AddScript(name string, data []byte) error // add a command script to lib
	GetScript(name string) ([]byte, error)    // get a script from lib
	ListScript() []string
}

// Runner :an interface for runner
type Runner interface {
	Execute(args ...string) (stdOut string, stdErr string, err error)
}
