// +build !windows

package workspace

import (
	"bytes"
	"os/exec"
)

/*
	a shell execution engine for linux


*/

var shell = "/usr/local/bin/bash"

type Shell struct {
	Shell string
	Dir   string
}

func NewShell(dir string) *Shell {
	//ps, _ := exec.LookPath("powershell.exe")
	return &Shell{
		Shell: shell,
		Dir:   dir,
	}
}

func (p *Shell) Execute(args ...string) (stdOut string, stdErr string, err error) {
	//args = append([]string{"-NoProfile", "-NonInteractive"}, args...)
	//ex := args[0]
	//args = args[1:]
	cmd := exec.Command(p.Shell, args...)
	if p.Dir != "" {
		cmd.Dir = p.Dir
	}
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	err = cmd.Run()
	stdOut, stdErr = stdout.String(), stderr.String()
	return
}
