package config_test

import (
	"testing"

	"bitbucket.org/orangeparis/runner/config"
	"github.com/spf13/viper"
)

func TestConfig(t *testing.T) {

	config.Load()
	data := viper.AllSettings()
	_ = data

	tmp := viper.GetString("vars.tmp")
	//println(tmp)
	if tmp != "/tmp" {
		t.Errorf("tmp was incorrect, got: %s, want: %s", tmp, "/tmp")
	}

	// setting vars.tmp
	viper.Set("vars.tmp", "/tmp/modified")

	tmp = viper.GetString("vars.tmp")
	//println(tmp)
	if tmp != "/tmp/modified" {
		t.Errorf("repo was incorrect, got: %s, want: %s", tmp, "/tmp/modified")
	}

	app := viper.GetString("app")
	if app != "runner" {
		t.Errorf("app was incorrect, got: %s, want: %s", app, "runner")
	}

	//println(viper.GetString("app"))

}
