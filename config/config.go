package config

import (
	"fmt"

	"github.com/spf13/viper"
)

type Configuration struct {
	Name string
}

func init() {

	// defaults
	viper.SetDefault("appName", "runner")
	viper.SetDefault("RepositoryDir", "/tmp")
	viper.SetDefault("RepositoryPrefix", "runner") // temporary dir : /tmp/runner*

	// [web]
	viper.SetDefault("web.port", "8081")
	viper.SetDefault("web.static", "./static")
	viper.SetDefault("web.templates", "./templates")

	// config files
	viper.AddConfigPath("/etc/runner/")  // path to look for the config file in
	viper.AddConfigPath("$HOME/.runner") // call multiple times to add many search paths
	viper.AddConfigPath(".")             // optionally look for config in the working directory

	// [commands]
	viper.SetDefault("commands.commands", "/tmp/commands")
	viper.SetDefault("commands.run", "/tmp/runs")

	// Env vars
	viper.SetEnvPrefix("runner")

}

// Load : read the config files
func Load() {

	// read main "config.toml" file
	viper.SetConfigName("config") // name of config file (without extension)
	err := viper.ReadInConfig()   // Find and read the config file
	if err != nil {               // Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %s", err))
	}

}

// Get : Get config key
func Get(key string) interface{} {
	return viper.Get(key)
}

// Set config key with value
func Set(key string, value interface{}) {
	viper.Set(key, value)
}

// GetString : get key as string
func GetString(key string) string {
	return viper.GetString(key)
}
