package app

import (
	"html/template"
	"log"
	"net/http"
)

func HomeHandler(w http.ResponseWriter, r *http.Request) {

	t, err := template.New("home.html").ParseFiles(TemplatePath("home"))
	if err != nil {
		log.Print("template compiling error: ", err)
	}

	err = t.Execute(w, nil)
	if err != nil {
		log.Print("template executing error: ", err)
	}

}
