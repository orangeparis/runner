package app

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/orangeparis/runner/workspace"
)

// CreateRunIn message to create a run   ( POST /api/v1/run/ )
type CreateRunIn struct {
	Command string
}

// RunCommandIn message to run a command   ( POST /api/v1/run/<run_id> )
type RunCommandIn struct {
	Command string
	args    string
}

// RunHandler : handle  /api/v1/run/*
func RunHandler(w http.ResponseWriter, r *http.Request) {

	base := "/api/v1/run/"

	if len(r.URL.Path) <= len(base) {
		// this is a collection command  /api/v1/run ( create , List )
		switch r.Method {
		case http.MethodPost:
			CreateRun(w, r)
		case http.MethodGet:
			ListRun(w, r)
		default:
			http.Error(w, "RUNS method not allowed", http.StatusMethodNotAllowed)
		}
	} else {
		// this is an item command /api/v1/run/<run_id>
		runId := r.URL.Path[len(base):]
		switch r.Method {
		case http.MethodPost:
			// this is a run commnand  POST /api/v1/run/<run_id>
			RunCommand(runId, w, r)
		case http.MethodGet:
			// this is a run commnand  GET /api/v1/run/<run_id>
			GetRun(runId, w, r)
		case http.MethodDelete:
			// this is a run commnand  DELETE /api/v1/run/<run_id>
			DeleteRun(runId, w, r)
		default:
			http.Error(w, "Run method not allowed", http.StatusMethodNotAllowed)
		}
	}
	return

}

//
// collection runs
//

// CreateCommandHandler : create a Run a POST /api/v1/run/<run_id>
//   json parameters  { "command": }
func CreateRun(w http.ResponseWriter, r *http.Request) {

	// extract command from json parameters
	decoder := json.NewDecoder(r.Body)
	var in CreateRunIn
	err := decoder.Decode(&in)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	log.Printf("create a workspace for command: %s", in.Command)

	// get the workspace pool
	pool, err := workspace.WorkSpacePoolFromConfig()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	// create the workspace
	wk, err := pool.NewSpace(in.Command, "")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	runId := wk.Name()

	// return runId as plain text
	w.Header().Set("Content-Type", "text/plain")
	fmt.Fprintf(w, runId)
	return

}

func ListRun(w http.ResponseWriter, r *http.Request) {

	http.Error(w, "List Run Not Implemented", http.StatusNotImplemented)
}

//
// item run
//

// RunCommandHandler : Run a command /api/v1/run/<run_id>
func RunCommand(runId string, w http.ResponseWriter, r *http.Request) {

	// extract command from json parameters
	decoder := json.NewDecoder(r.Body)
	var in RunCommandIn
	err := decoder.Decode(&in)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// get the workspace pool
	pool, err := workspace.WorkSpacePoolFromConfig()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	// get the workspace
	wk, err := pool.Space(runId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	// run the command into the workspace
	err = wk.Run(in.Command, []string{})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Error(w, "OK", http.StatusOK)
}

// GetRun : get result and status of the run  GET /api/v1/run/<run_id>
func GetRun(runId string, w http.ResponseWriter, r *http.Request) {

	http.Error(w, "Get RUN Not Implemented", http.StatusNotImplemented)
}

// DeleteRun : delet run workspace  DELETE /api/v1/run/<run_id>
func DeleteRun(runId string, w http.ResponseWriter, r *http.Request) {

	http.Error(w, "Delete RUN Not Implemented", http.StatusNotImplemented)
}

// memo
//io.WriteString(w, "This is a get request")
