package app

import (
	"net/http"
)

func Routes() {

	//
	// static file server
	//
	// This works and strip "/static/" fragment from path
	fs := http.FileServer(http.Dir(StaticPath("")))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	//
	// home
	//
	http.HandleFunc("/", HomeHandler)

	// api run  /api/v1/run/
	http.HandleFunc("/api/v1/run/", RunHandler)

	// api upload  /api/v1/upload/
	http.HandleFunc("/api/v1/upload/", UploadHandler)

	// api commands  /api/v1/commands/
	http.HandleFunc("/api/v1/commands/", CommandsHandler)

}
