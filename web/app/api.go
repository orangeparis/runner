package app

import (
	"bufio"
	"io/ioutil"
	"log"
	"net/http"
	"path/filepath"
	"strings"

	"bitbucket.org/orangeparis/infralis/config"
	repo "bitbucket.org/orangeparis/infralis/infralis"
	sheets "bitbucket.org/orangeparis/infralis/sheets"
)

/*

	handle api

	GET /api/csv/<file.csv>

	POST /api/update/<name.csv>   data: csv
	POST /api/commit    data { message, user, pwd }



*/

// ApiCsvHandler : handle  /api/csv/<name>    eg /api/csv/pdu-fa13.csv
// return csv content
func ApiCsvHandler(w http.ResponseWriter, r *http.Request) {

	name := r.URL.Path[len("/api/csv/"):]
	data, err := repo.GetPlatform(name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	sheet, err := sheets.NewSheet(name)
	err = sheet.LoadFromCsv(data, ',')
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	sheet.AddLeftColumn("Select", "")
	csv, err := sheet.ToCsv(',')

	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte(csv))
}

// ApiUpdateHandler : handle  /api/update/<filename>    eg /api/update/pdu-fa13.csv
// update the file
func ApiUpdateHandler(w http.ResponseWriter, r *http.Request) {

	// extract short name of the file eg pdu-fa13.csv
	name := r.URL.Path[len("/api/update/"):]
	// wee need to add the headers
	tables, err := config.NewTables()
	if err != nil {
		log.Printf("cannot read config\n")
	}
	cnf := tables.GetConfig(name)
	var headers string
	if cnf != nil {
		headers = strings.Join(cnf.Headers, ",") + "\n"
	}

	var content string
	if headers != "" {
		// add headers
		content = headers
	}

	// compute full path
	root := repo.Root()
	filename := filepath.Join(root, name)
	// read the csv content of the file
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("cannot read body\n")
	}
	sr := strings.NewReader(string(data))
	source := bufio.NewReader(sr)
	source2 := bufio.NewReader(r.Body)
	_ = source2

	// add each line ( without the first field (select))
	for {
		line, err := source.ReadString('\n')

		if err != nil {
			break
		}
		mline := line[2:]
		content = content + mline
		println(string(content))

	}

	//content = append(content, data...)
	//println(string(content))

	// write the whole body at once
	err = ioutil.WriteFile(filename, []byte(content), 0644)
	if err != nil {
		log.Printf("cannot write:%s\n", err.Error())
	}
	return

	//path := filepath.join(root, name)

	// w.Header().Set("Content-Type", "text/plain")
	// w.Write([]byte(csv))
}

// ApiCommitHandler : handle  POST /api/commit
// commit the repository  (  git commit -a -m "message" )
func ApiCommitHandler(w http.ResponseWriter, r *http.Request) {

	text := "Not yet implemented"

	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte(text))
}
