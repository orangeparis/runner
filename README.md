# runner #

a simple http server to run predefine commands


# create a run instance

	POST /api/v1/run  { cmd:  user: }  
	
return a run_id in plain text

# Add file to a run instance

	POST /api/v1/upload/<run_id>/<filename>   content
	
# launch command

	POST /api/v1/run/<run_id>
	
# read run result

	GET /api/v1/run/<run_id>


# delete a run 

	DELETE /api/v1/run/<run_id>
	
# list runs

	GET /api/v1/run
	
# list commands

	GET /api/v1/commands


